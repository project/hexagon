<?php


/**
 * Returns the current active theme. Use this instead of messing with the
 * global variable.
 */
function hex_active_theme() {
  global $theme_key;
  return $theme_key;
}

/**
 * Returns an array of active themes.
 * 
 * @param $sort_by_subtheme
 *  This sorts the list of active themes beginning with sub-themes. Pass a
 *  FALSE value to sort with the base theme first.
 */
function hex_active_themes($sort_by_subtheme = TRUE) {
  return hex_theme_data('name', $sort_by_subtheme, NULL);
}

/**
 * Return the path to a theme.
 * 
 * This only works for active themes. In other words, sub-themes that are
 * actively running or any of its base themes.
 * 
 * @param $theme
 *  (optional) If this is set, it will return its path. Without it, it will
 *  return the path to the active theme. This is not the same as core's
 *  path_to_theme() function. That function can lead to anywhere since it can
 *  point to the owner of a themed template file depending on the scope of
 *  where it's called. This will always return the path to one of the themes
 *  connected to Hexagon and it is completely independent of templates.
 */
function hex_theme_path($theme = NULL) {
  if (!isset($theme)) {
    $theme = hex_active_theme();
  }
  $theme_files = hex_theme_data('filename');

  return dirname($theme_files[$theme]);
}

/**
 * Pass in a .info key set for determining inheritance.
 * 
 * @return
 *  Array of theme keys.
 */
function hex_theme_inheritance_for_infokey($info_key) {

  $active_theme = hex_active_theme();
  $active_themes = $_themes = hex_active_themes(FALSE);
  // From base to sub-themes. Collect inherit keys.
  foreach ($active_themes as $theme_key) {
    // This subtraction will prevent a base theme from setting a child theme as a source of inheritance.
    unset($_themes[$theme_key]);
    $inherit_keys = hex_theme_info($info_key, array(), $theme_key);
    if (is_string($inherit_keys)) {
      $inherit_keys = array($inherit_keys);
    }
    foreach ($inherit_keys as $inherit_key) {
      if ($inherit_key != $active_theme && isset($active_themes[$inherit_key]) && !isset($_themes[$inherit_key])) {
        $theme_keys[$inherit_key] = $inherit_key;
      }
    }
  }
  // Active theme always added and added last.
  $theme_keys[$active_theme] = $active_theme;

  return $theme_keys;
}

/**
 * Get the values defined within the .info file.
 * 
 * @param $info_key
 *  (required) The key to retrieve.
 * @param $default
 *  Fall back value if nothing is found.
 * @param $theme
 *  Theme specific value. If not set, it will return the value for the active
 *  theme.
 */
function hex_theme_info($info_key, $default = NULL, $theme = NULL) {

  if (!isset($theme)) {
    $theme = hex_active_theme();
  }
  $theme_data = hex_theme_data('info');
  if (isset($theme_data[$theme][$info_key])) {
    $theme_info = $theme_data[$theme][$info_key];
  }
  // Pass through defauts if nothing found.
  if (!isset($theme_info)) {
    $theme_info = $default;
  }

  return $theme_info;
}

/**
 * Shortcut for theme_get_setting. This will also fallback to the 'settings'
 * key found within the active themes .info file. An optional default can be
 * passed just in case no value is found.
 */
function hex_setting($setting_key, $default = NULL) {
  // Core's settings api.
  $theme_setting = theme_get_setting($setting_key);

  if (!isset($theme_setting)) {
    // Fallback to the active theme's .info.
    $info_settings = hex_theme_info('settings', array());
    if (isset($info_settings[$setting_key])) {
      // Check for specific setting.
      // .info setting key = setting[SETTING_KEY] = ...
      $theme_setting = $info_settings[$setting_key];
    }
  }

  return isset($theme_setting) ? $theme_setting : $default;
}

/**
 * Returns an array with specific theming meta data keyed to the theme name.
 * 
 * @param $object
 *  Points to an object within the themes meta data.
 * @param $sort_by_subtheme
 *  Sort by sub-themes to base themes when TRUE. This is what we want 99% of
 *  the time.
 */
function hex_theme_data($object, $sort_by_subtheme = TRUE) {
  global $theme_info, $base_theme_info;

  if (!empty($base_theme_info)) {
    foreach ($base_theme_info as $base_info) {
      $theme_data[$object][$base_info->name] = $base_info->$object;
    }
  }
  $theme_data[$object][$theme_info->name] = $theme_info->$object;

  return $sort_by_subtheme ? array_reverse($theme_data[$object]) : $theme_data[$object];
}

