<?php


/**
 * Clean strings. Mainly used for HTML attributes like classes. Also used for
 * template names to keep them consistent.
 * 
 * @param $string
 *  The string to be cleaned.
 * @param $filter
 *  String or array of strings used for search.
 * @param $replace
 *  String or array of strings used for replacement.
 */
function hex_clean_string($string, $filter = array(':', '/', '_', ' '), $replace = '-') {
  return strtolower(str_replace($filter, $replace, $string));
}

/**
 * Pass in an element and value will filter the element appropriately.
 */
function hex_filter_attributes($element, $value) {
  // Filter strings. They will be replaced with a hyphen.
  $filters = array(
    'id' => array(':', '/', '_', ' '),
    'class' => array('_'),
  );
  if (isset($filters[$element])) {
    $value = hex_clean_string($value, $filters[$element]);
  }
  return $value;
}

/**
 * Can return width/height/type/bits/channels (int) and dimensions/mime (string).
 * Not all data available for all image types.
 * 
 * @see getimagesize()
 */
function hex_image_info($image, $info = 'dimensions') {
  $map_attr = array('width' => 0, 'height' => 1, 'type' => 2, 'dimensions' => 3, 'bits' => 'bits', 'channels' => 'channels', 'mime' => 'mime');
  $img_info = @getimagesize($image);
  return !empty($img_info[$map_attr[$info]]) ? $img_info[$map_attr[$info]] : '';
}

/**
 * Convert select string into a flat array.
 */
function _hex_select_string_to_array($selection) {
  return !empty($selection) ? explode('>', $selection) : array();
}

/**
 * Bind references in deep array/object structures.
 */
function &_hex_map_reference(&$elements, &$selection) {
  // Keep traversing in recursive calls. Always maintain reference.
  foreach ($selection as $i => $select) {
    // Treat arrays and objects the same for consistency.
    if (is_array($elements) && isset($elements[$select])) {
      $element_child = &$elements[$select];
    }
    elseif (is_object($elements) && isset($elements->$select)) {
      $element_child = &$elements->$select;
    }
    else {
      // A miss. Break out and do nothing.
      $return_element = FALSE;
      break;
    }
    // Recurse.
    unset($selection[$i]);
    $return_element = &_hex_map_reference($element_child, $selection);
  }

  // If $return_element is not set, then the target was found.
  if (!isset($return_element)) {
    $return_element = &$elements;
  }
  return $return_element;
}

/**
 * Print selected element. Use only in templates.
 */
function print_select(&$elements, $selection = '') {
  print return_select($elements, $selection);
}

/**
 * Process elements and return.
 */
function return_select(&$elements, $selection = '') {
  if (strpos($selection, '#') !== FALSE) {
    list($selection, $hash_key) = explode('#', $selection);
  }
  $selection = _hex_select_string_to_array($selection);
  // Reference to nested target.
  $target_element = &_hex_map_reference($elements, $selection);
  // Look for #HASH_KEY and process further.
  if ($target_element !== FALSE && isset($hash_key)) {
    // $sub_hash_key == #HASH_KEY.SUB_HASH_KEY
    $sub_hash_key = FALSE;
    if (strpos($hash_key, '.') !== FALSE) {
      $sub_hash_key = explode('.', $hash_key);
      $hash_key = array_shift($sub_hash_key);
    }
    // $hash_key is used to invoke other functions.
    // Example: #attributes will invoke '_hex_process_attributes()'.
    if (function_exists($process_hash = '_hex_process_' . $hash_key)) {
      $return = $process_hash($target_element, $sub_hash_key);
    }
  }
  // Else return target.
  else {
    $return = $target_element;
  }
  return $return;
}

/**
 * Process html attributes within #attributes key. Invoked from print_select
 * dynamically.
 */
function _hex_process_attributes(&$element, $sub_attributes = array()) {
  // Process all attributes only once.
  if (!(isset($element['#attributes_processed']) && $element['#attributes_processed'])) {
    if (!empty($element['#attributes'])) {
      foreach ($element['#attributes'] as $attr => $value) {
        if ($value) {
          $value = is_array($value) ? implode(' ', $value) : $value;
          $value = hex_filter_attributes($attr, $value);
          $element['#attributes.' . $attr] = $value;
          $element['#attributes.' . $attr . '+'] = ' ' . $attr . '="' . $value . '"';
        }
      }
    }
    $element['#attributes_processed'] = TRUE;
  }

  $output = '';
  if (!empty($element['#attributes'])) {
    if (empty($sub_attributes)) {
      $sub_attributes = array_keys($element['#attributes']);
      $force_name_value_pair = TRUE;
    }
    // Can't return multiple attributes without a name value pair.
    // id="xx", class="xx", NAME="VALUE", etc...
    elseif (count($sub_attributes) > 1) {
      $force_name_value_pair = TRUE;
    }

    foreach ($sub_attributes as $select) {
      if (!empty($force_name_value_pair) && strpos($select, '+') === FALSE) {
        $select .= '+';
      }
      if (isset($element['#attributes.' . $select])) {
        $output .= $element['#attributes.' . $select];
      }
    }
  }

  return $output;
}

/**
 * Generates opening element.
 */
function _hex_process_element_open(&$element) {
  if (!isset($element['#element_open'])) {
    $element['#element_open'] = '';
    $attributes = _hex_process_attributes($element);
    // Default to a div if tag is not set but contains attributes.
    // If no tag or attributes are set, this will return nothing.
    if (!empty($attributes) && empty($element['#element_tag'])) {
      $element['#element_tag'] = 'div';
    }
    if (!empty($element['#element_tag'])) {
      $element['#element_open'] = '<' . $element['#element_tag'] . $attributes . '>';
    }
  }
  return isset($element['#element_open']) ? $element['#element_open'] : '';
}

/**
 * Generates closing element.
 */
function _hex_process_element_close(&$element) {
  if (!isset($element['#element_close']) && !empty($element['#element_tag'])) {
    $element['#element_close'] = '</' . $element['#element_tag'] . '>';
  }
  return isset($element['#element_close']) ? $element['#element_close'] : '';
}

/**
 * Alter selected array/object member.
 */
function alter_select(&$elements, $alt_value, $selection = '') {
  $selection = _hex_select_string_to_array($selection);
  if ($target_element = &_hex_map_reference($elements, $selection)) {
    $target_element = $alt_value;
  }
  return $target_element;
}

