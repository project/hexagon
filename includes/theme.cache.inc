<?php

/**
 * Anything related to caching placed here. It will be included only when the
 * caches need to be rebuilt. See the theme's template.php file.
 */


/**
 * THEME REGISTRY ALTERATIONS.
 * ---------------------------------------------------------------------------
 */


/**
 * Rebuild and save the modified theme registry.
 * 
 * This allows the theme to get the whole scope of the registry unlike
 * HOOK_theme(). It shouldn't break anything and the rest of the theming system
 * will work as it's designed.
 * 
 * This will be called before cores' _theme_load_registry() and allow it to
 * function returning a modified version of the registry. All files should be
 * included to allow discovery of theme functions and variable processors.
 * 
 * The core function _theme_build_registry() will not be invoked. This takes
 * its place.
 * 
 * Make sure the following functions are called before rebuilding the registry.
 * 
 * @see drupal_rebuild_theme_registry()
 * @see hex_clear_cache()
 */
function _hex_build_registry() {
  global $theme_info, $base_theme_info, $theme_engine;
  $theme = $theme_info;
  $base_theme = $base_theme_info;

  // This is very similar to _theme_build_registry().
  // Done this way so drupal_alter() can affect themes and plug-ins equally, not just modules.
  $cache = array();
  // First, process the theme hooks advertised by modules. This will
  // serve as the basic registry.
  foreach (module_implements('theme') as $module) {
    _hex_process_registry($cache, $module, 'module', $module, drupal_get_path('module', $module));
  }
  // Process each base theme.
  foreach ($base_theme as $base) {
    // If the base theme uses a theme engine, process its hooks.
    if ($theme_engine) {
      _hex_process_registry($cache, $theme_engine, 'base_theme_engine', $base->name, dirname($base->filename));
    }
    _hex_process_registry($cache, $base->name, 'base_theme', $base->name, dirname($base->filename));
  }
  // And then the same thing, but for the theme.
  if ($theme_engine) {
    _hex_process_registry($cache, $theme_engine, 'theme_engine', $theme->name, dirname($theme->filename));
  }
  // Finally, hooks provided by the theme itself.
  _hex_process_registry($cache, $theme->name, 'theme', $theme->name, dirname($theme->filename));
  // Allow HOOK_theme() registry alterations from plug-in's.
  foreach (hex_plugin_list() as $plugin) {
    if ($pinfo_file = hex_path_to_file($plugin . '.pinfo')) {
      $known_hooks = $cache;
      _theme_process_registry($cache, $plugin, 'plugin', $plugin, dirname($pinfo_file));
      $hook_owners[$plugin] = array_keys(array_diff_assoc($cache, $known_hooks));
    }
  }

  // Let modules alter the registry
  drupal_alter('theme_registry', $cache);
  // Allow themes and plug-ins equal control. Similar to HOOK_theme_registry_alter() available to modules.
  foreach (array_merge(hex_active_themes(FALSE), hex_plugin_list()) as $component) {
    if (function_exists($reg_alter = $component . '_theme_registry_alter')) {
      $reg_alter($cache);
    }
  }

   _theme_save_registry($theme_info, $cache);

}

/**
 * Helper function for processing the registry.
 * 
 * This intermediate function exists to detect the owners of each theming hook.
 */
function _hex_process_registry(&$cache, $name, $type, $theme, $path) {
  $known_hooks = $cache;
  _theme_process_registry($cache, $name, $type, $theme, $path);
  _hex_hook_owner($name, array_keys(array_diff_assoc($cache, $known_hooks)));
}

/**
 * Adds missing preprocess functions for theme defined template hooks and adds
 * two extra variable process phases.
 * 
 * Plug-in's with their own variable processors are also included here.
 * 
 * This also injects file paths to include
 * "MODULE/THEME/PLUGIN.common-vars.inc" files when he hook is called.
 * 
 * And finally, each template hook adds additional theme paths so template
 * suggestions do not depend on the base template for a hook for it to be
 * noticed.
 */
function hex_theme_registry_alter(&$registry) {

  // Gather a list of modules, theme's and plugins then get their paths.
  $list_by_type = array(
    'module' => module_list(),
    'theme'  => hex_active_themes(FALSE),
  );
  $type_paths = array();
  foreach ($list_by_type as $type_key => $type_items) {
    foreach ($type_items as $type_item) {
      $type_paths[$type_item] = drupal_get_path($type_key, $type_item);
    }
  }
  $list_by_type['plugin'] = hex_plugin_list();
  foreach ($list_by_type['plugin'] as $plugin) {
    $type_paths[$plugin] = hex_plugin_path($plugin);
  }

  // Gather prefixes in the correct order.
  $prefixes = array('template');
  $prefixes += $list_by_type['module'];
  $prefixes += $list_by_type['theme'];
  $prefixes += $list_by_type['plugin'];

  foreach ($registry as $hook => $info) {
    if (isset($info['function']) && !isset($info['template']) && isset($info['preprocess functions'])) {
      // Minor cleaning. Functions don't need preprocess functions.
      unset($registry[$hook]['preprocess functions']);
    }
    elseif (isset($info['template'])) {
      /**
       * ADD VARIABLE PROCESSORS.
       * 
       * Ordered into three phases.
       */
      $preprocess_functions = array();
      foreach (array('pre_preprocess', 'preprocess', 're_process') as $phase) {
        foreach ($prefixes as $prefix) {
          if (function_exists($prefix . '_' . $phase)) {
            $preprocess_functions[] = $prefix . '_' . $phase;
          }
          if (function_exists($prefix . '_' . $phase . '_' . $hook)) {
            $preprocess_functions[] = $prefix . '_' . $phase . '_' . $hook;
          }
        }
        if ($phase == 'preprocess') {
          // Merge with existing since modules might add preprocessors in a non-standard way.
          $preprocess_functions = array_merge($preprocess_functions, $registry[$hook]['preprocess functions']);
        }
      }
      // Force uniqueness and set back to registry.
      $registry[$hook]['preprocess functions'] = array_values(array_unique($preprocess_functions));

      // Post renderers.
      $post_render_functions = array();
      foreach ($prefixes as $prefix) {
        if (function_exists($prefix . '_post_render_template')) {
          $post_render_functions[] = $prefix . '_post_render_template';
        }
        if (function_exists($prefix . '_post_render_template_' . $hook)) {
          $post_render_functions[] = $prefix . '_post_render_template_' . $hook;
        }
      }
      if (!empty($post_render_functions)) {
        $registry[$hook]['post render functions'] = $post_render_functions;
      }

      /**
       * APPEND THEMING PATHS LEADING TO TEMPLATE SUGGESTIONS.
       * 
       * Add in extra theming paths leading to template suggestions. This
       * allows template suggestions to be used without the existence of the
       * base template in the same location.
       */
      // First create a filter so templates beginning with a similar prefix as the current hook is not mistakenly added.
      $tpl_filter = array();
      foreach (array_keys($registry) as $key) {
        if (preg_match('/^' . $hook . '.+$/', $key)) {
          $tpl_filter[] = '^' . str_replace('_', '-', $key);
        }
      }
      $tpl_filter_pattern = !empty($tpl_filter) ? '/(' . implode('|', $tpl_filter) . ').*\.tpl\.php$/' : FALSE;
      // This mask searches for template suggestions only assuming the suggestion is prefixed with the original hook.
      $mask = '^' . str_replace('_', '-', $hook) . '.+\.tpl\.php$';
      // Loop from base to sub-themes.
      foreach (hex_active_themes(FALSE) as $theme_key) {
        // Add in filters to ignore while scanning. Base themes must ignore sub-theme directories.
        if ($scan_data = file_scan_directory(hex_theme_path($theme_key), $mask, hex_nomask($theme_key, 'theme'))) {
          foreach ($scan_data as $full_path => $file_data) {
            if (!in_array(dirname($full_path), $info['theme paths']) && !($tpl_filter_pattern && preg_match($tpl_filter_pattern, $file_data->basename))) {
              // Paths ordered from modules to themes.
              $registry[$hook]['theme paths'][] = dirname($file_data->filename);
            }
          }
        }
      }

      /**
       * DYNAMIC SUPPORT FILE INCLUSION BASED ON ORIGINATING MODULE/PLUG-IN/THEME.
       * 
       * Add in support for MODULE|THEME|PLUGIN_NAME.common-vars.inc files or
       * HOOK.vars.inc file so they are included on the fly. They are added
       * from hex_pre_preprocess().
       */
      $owner_key = _hex_hook_owner($hook);
      // Add the owner of the current hook, e.g., system, node, block, etc...
      $registry[$hook]['owner'] = $owner_key;
      $include_patterns = array(
        '/^' . str_replace('_', '(-|_)', $owner_key) . '\.common(-|_)vars\.inc$/',
        '/^' . str_replace('_', '(-|_)', $hook) . '.vars.inc$/',
      );
      $hex_includes = array();
      foreach ($include_patterns as $include_pattern) {
        if ($includes = hex_path_to_files($include_pattern, NULL, FALSE, TRUE)) {
          $hex_includes = array_merge($includes, $hex_includes);
        }
      }
      if (!empty($hex_includes)) {
        $registry[$hook]['includes'] = $hex_includes;
      }
    }
  }

}


/**
 * THEME RESOURCES
 * ---------------------------------------------------------------------------
 */


/**
 * Files to be cached.
 */
function _hex_build_files_data() {

  $file_match_data = array();
  $plugins = _hex_get_plugin_data();

  // Loop from base themes to their child sub-themes. Very important!
  // Build data for scanning files.
  foreach (hex_active_themes(FALSE) as $theme_key) {
    // Add plug-in data first right before the owner -theme that contains the plug-in.
    foreach ($plugins as $plugin_key => $plugin_data) {
      // Skip if not the current owner.
      if ($plugin_data->info['owner'] != $theme_key) {
        continue;
      }
      $file_match_data[$plugin_key] = array(
        '_path'   => dirname($plugin_data->filename),
        '_type'   => 'plugin',
        '_owner'  => $plugin_data->info['owner'],
        '_match'  => !empty($plugin_data->info['match']) ? $plugin_data->info['match'] : array(),
        '_ignore' => !empty($plugin_data->info['ignore']) ? $plugin_data->info['ignore'] : array(),
      );
      // Default matching patterns for plug-ins.
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.plug\.inc$';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.cache\.inc$';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.css';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.js';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.common(-|_)func\.inc$';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.common(-|_)vars\.inc$';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.func\.inc$';
      $file_match_data[$plugin_key]['_match']['patterns'][] = '.*\.vars\.inc$';
      // This is already detected. Set on the ignore list.
      $file_match_data[$plugin_key]['_ignore']['patterns'][] = '.*\.pinfo$';
    }
    // Now the theme.
    $file_match_data[$theme_key] = array(
      '_path'   => hex_theme_path($theme_key),
      '_type'   => 'theme',
      '_owner'  => $theme_key,
      '_match'  => hex_theme_info('match', array(), $theme_key),
      '_ignore' => hex_theme_info('ignore', array(), $theme_key),
    );
    // Default matching patterns for themes.
    $file_match_data[$theme_key]['_match']['files'][] = 'template.php';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.cache\.inc$';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.css';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.js';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.common(-|_)func\.inc$';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.common(-|_)vars\.inc$';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.func\.inc$';
    $file_match_data[$theme_key]['_match']['patterns'][] = '.*\.vars\.inc$';
    // Template (.tpl.php) files are already stored in the theme registry. Ignore them here.
    $file_match_data[$theme_key]['_ignore']['patterns'][] = '.*\.tpl\.php$';
  }

  /**
   * Process scanning.
   *
   * Possible "match" and "ignore" types pulled from .info and .pinfo:
   *  'patterns' - regex patterns
   *  'files'    - file names. literal match.
   */

  $resource_files = $carryover_mask = array();
  foreach ($file_match_data as $key => $data) {
    /**
     * extraction will expose the following variables:
     *  - $_path
     *  - $_type
     *  - $_owner
     *  - $_match
     *  - $_ignore
     */
    extract($data, EXTR_OVERWRITE);

    if (!empty($_match) || ($_type == 'theme' && !empty($carryover_mask))) {

      $mask = array();
      // This will prevent scanning of dependent plug-in's folder or child theme folder.
      $nomask = hex_nomask($key, $_type);

      if ($_type == 'theme') {
        // Previously scanned items can be inherited into themes only.
        $mask = $carryover_mask;
        // Ignore plug-ins folder for themes. It will be scanned separately.
        $nomask[] = 'plug-ins';
      }

      // Regex pattern match.
      if (!empty($_match['patterns'])) {
        $mask = array_merge($mask, $_match['patterns']);
      }
      // Exact file match.
      if (!empty($_match['files'])) {
        foreach ($_match['files'] as $file) {
          $mask[] = '^' . str_replace('.', '\.', $file) . '$';
        }
      }

      $ignore = array();
      // Regex pattern ignore.
      if (isset($_ignore['patterns'])) {
        $ignore = $_ignore['patterns'];
      }
      // Exact file ignore.
      if (isset($_ignore['files'])) {
        foreach ($_ignore['files'] as $ignore_file) {
          $ignore[] = '^' . str_replace('.', '\.', $ignore_file) . '$';
        }
      }

      // Start scanning.
      if (!empty($mask) && $scan_data = file_scan_directory($_path, implode('|', $mask), $nomask)) {
        foreach ($scan_data as $full_path => $file_data) {
          // Skip files in the ignore array.
          if ($ignore && ereg(implode('|', $ignore), $file_data->basename)) {
            continue;
          }
          $resource_files[$file_data->basename][$_owner][] = $full_path;
          // This will be added to $mask in subsequent loops to allow drag and drop overriding
          // without the need to define new files or patterns from the .info file.
          $carryover_mask[] = '^' . str_replace('.', '\.', $file_data->basename) . '$';
        }
      }
    }
  }

  return $resource_files;
}


/**
 * THEME PLUG-INS
 * ---------------------------------------------------------------------------
 */


/**
 * Returns plug-in data only for active plug-in's.
 */
function _hex_build_plugin_data() {

  // Get enabled plug-in's as they are defined from the active theme.
  // There is no inheritance for the state of plug-in's set from .info.
  $activated_plugin_keys = hex_theme_info('plugins', array());

  // Gather and build data of all available plug-in's held in each active theme.
  $all_plugins = array();

  // From base to sub-themes.
  foreach (hex_active_themes(FALSE) as $theme_key) {
    // Confine to each theme. Prevent parent themes from digging into their
    // sub-themes. Each will be processed separately.
    foreach (file_scan_directory(hex_theme_path($theme_key), '.*\.pinfo$', hex_nomask($theme_key, 'theme')) as $plugin_key => $plugin_data) {
      // Keep plug-in key consistent.
      $plugin_key = $plugin_data->name;
      // Parse .pinfo meta data and set owner.
      $info = drupal_parse_info_file($plugin_data->filename);
      $info['owner'] = $theme_key;
      if (!empty($info['dependencies'])) {
        foreach ($info['dependencies'] as $i => $dependency) {
          // Keep it clean and consistent just like the main plug-in key.
          $info['dependencies'][$i] = $dependency;
        }
        // Defined dependencies separated from dependencies since _module_build_dependencies()
        // fills in multi-level dependencies. This will be used for setting proper order.
        $info['defined_dependencies'] = $info['dependencies'];
      }
      $all_plugins[$plugin_key] = $plugin_data;
      $all_plugins[$plugin_key]->info = $info;
    }
  }

  // This core function was made for modules but it's generic enough to use
  // for plug-in's. It builds dependency data to be processed below.
  $all_plugins = _module_build_dependencies($all_plugins);
  // Build list of missing dependencies.
  // TODO: Add a notice when dependencies are missing to avoid confusion.
  // If any of the dependencies are missing, the theme enabled plug-in
  // will not load at all. Currently, it's silent about it.
  // 
  // The error should be given in the space provided in the next two blocks.
  $missing_dependencies = array();
  foreach ($all_plugins as $plugin_key => $plugin_data) {
    if (!empty($plugin_data->info['dependencies'])) {
      foreach ($plugin_data->info['dependencies'] as $dependency) {
        if (!isset($all_plugins[$dependency])) {
          $missing_dependencies[$plugin_key][] = $dependency;
        }
      }
    }
  }

  // Build list of plug-ins there were explictly enabled by the active theme.
  $activated_plugins = array();
  foreach ($activated_plugin_keys as $active_plugin) {
    if (isset($all_plugins[$active_plugin]) && empty($missing_dependencies[$active_plugin])) {
      $activated_plugins[$active_plugin] = $all_plugins[$active_plugin];
    }
    else {
      // TODO: Theme defined plug-in or dependency doesn't exist. Give error message?
    }
  }

  // Build list of plug-in dependencies. These are processed separately to guarentee a predictable order.
  $plugin_dependencies = array();
  foreach ($activated_plugins as $active_plugin => $active_plugin_data) {
    // This loop is set-up this way so any dependent plug-in's are listed before others.
    $_process_dependencies = !empty($active_plugin_data->info['defined_dependencies']) ? $active_plugin_data->info['defined_dependencies'] : array();
    while (!empty($_process_dependencies)) {
      foreach ($_process_dependencies as $i => $dependency) {
        if (isset($all_plugins[$dependency]) && empty($missing_dependencies[$dependency])) {
          $dependency_plugin_data = $all_plugins[$dependency];
          $plugin_dependencies[$dependency] = $dependency_plugin_data;
          // Add more dependencies. This will be processed recursively in the next foreach loop.
          if (!empty($dependency_plugin_data->info['defined_dependencies'])) {
            foreach ($dependency_plugin_data->info['defined_dependencies'] as $sub_dependency) {
              $_process_dependencies[] = $sub_dependency;
            }
          }
        }
        else {
          // TODO: Dependency is missing. Give error?
        }
        // Subtract processed plug-in.
        unset($_process_dependencies[$i]);
      }
    }
  }

  // Reverse dependency order so they come first.
  $plugins = array_reverse($plugin_dependencies) + $activated_plugins;

  // Finally, add active dependents or child plug-ins that are enabled.
  foreach ($plugins as $plugin_key => $plugin_data) {
    if (!empty($plugin_data->info['dependents'])) {
      foreach ($plugin_data->info['dependents'] as $dependent) {
        if (isset($plugins[$dependent])) {
          $plugins[$plugin_key]->info['active_dependents'][] = $dependent;
        }
      }
    }
  }

  return $plugins;
}

/**
 * Retrieve plug-in data.
 */
function _hex_get_plugin_data($reset = FALSE) {
  static $plugins;

  if (!isset($plugins) || $reset) {
    $plugins = _hex_build_plugin_data();
  }

  return $plugins;
}


/**
 * HELPERS
 * ---------------------------------------------------------------------------
 */


/**
 * Gather and return an array of hooks listed by their owners.
 * 
 * @see _hex_process_registry()
 */
function _hex_hook_owner($key = NULL, $hooks = NULL) {
  static $hook_owners;
  $value = '';

  // If setting:
  if (isset($key) && isset($hooks)) {
    foreach ($hooks as $hook) {
      $hook_owners[$hook] = $key;
    }
  }
  // Else if retrieving:
  elseif (isset($key) && isset($hook_owners[$key])) {
    $value = $hook_owners[$key];
  }
  elseif (!isset($key)) {
    $value = $hook_owners;
  }
  return $value;
}


/**
 * For the nomask paramater in file_scan_directory. Use it to prevents scanning
 * of the returned folder names.
 */
function hex_nomask($parent, $type) {
  $nomask = array('.', '..', 'CVS', '.svn');

  if ($type == 'theme') {
    foreach (list_themes() as $theme_key => $theme_data) {
      if (isset($theme_data->base_theme) && $theme_data->base_theme == $parent) {
        $theme_directory = dirname($theme_data->filename);
        $nomask[] = substr($theme_directory, strrpos($theme_directory, '/') + 1);
      }
    }
  }
  elseif ($type == 'plugin') {
    $plugins = _hex_get_plugin_data();
    if (!empty($plugins[$parent]->info['dependents'])) {
      foreach ($plugins[$parent]->info['dependents'] as $dependent) {
        if (isset($plugins[$dependent])) {
          $dependent_path = dirname($plugins[$dependent]->filename);
          $nomask[] = substr($dependent_path, strrpos($dependent_path, '/') + 1);
        }
      }
    }
  }

  return $nomask;
}

