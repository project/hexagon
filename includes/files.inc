<?php


/**
 * Pass in a file name to get its path. This will dig through all active themes
 * and plug-in's. This data is cached! Remember to clear it when needed.
 * 
 * This has a specific order in retrieving the file path. Sub-themes take
 * precedence then it will fall-back to any of its base themes or plug-in's. If
 * there is more than one file with the same name for the theme, this may not
 * be reliable. Make sure each file has a distinct name space.
 * 
 * All files for a given theme must have unique names.
 * 
 * The ability of this function to find the file depends depends on the how
 * match files and patterns are set from the .info (themes) and .pinfo (plug-ins).
 * 
 * @see _hex_build_files_data()
 *  Located within theme.cache.php.
 * 
 * @param $name
 *  The name of the file. The name of each file must be unique within each theme.
 * @param $theme
 *  Limit the result to a specific theme. Without this, it will dig through all
 *  active sub-themes and base themes until it finds a the file. This behavior
 *  changes with $search_theme_parents.
 * @param $search_theme_parents
 *  If set to TRUE, this depends on the $theme parameter to limit files to
 *  parent themes from what's set with $theme to any ancestors of that theme.
 *  Any files within child themes will be ignored.
 */
function hex_path_to_file($name, $theme = NULL, $search_theme_parents = FALSE) {
  $return_path = FALSE;
  if ($file_paths = hex_path_to_files($name, $theme, $search_theme_parents)) {
    $return_path = array_pop($file_paths);
  }

  return $return_path;
}

/**
 * Returns all file paths with a given name..
 * 
 * @see hex_path_to_file()
 *  Parameters are shared except for $regex.
 *
 * @param $regex
 *  Allows $name to use a regex pattern. Uses Perl compatible regular expressions.
 */
function hex_path_to_files($name, $theme = NULL, $search_theme_parents = FALSE, $regex = FALSE) {
  $return_paths = array();
  $resource_files = _hex_get_cache('files');

  if (!$regex && isset($theme) && !empty($resource_files[$name][$theme])) {
    $return_paths = $resource_files[$name][$theme];
  }
  else {
    $sub_limit = FALSE;
    foreach ($resource_files as $resource_name => $resource_group) {
      if (($regex && preg_match($name, $resource_name)) || (!$regex && $name == $resource_name)) {
        // Loops from base to sub-theme.
        foreach ($resource_group as $theme_key => $file_paths) {
          if ($sub_limit) {
            // Completely break out of the loop.
            break 2;
          }
          elseif (isset($theme) && $search_theme_parents && $theme_key == $theme) {
            // Prevent the next loop from processing of any child themes.
            // This is so base themes can't look ahead into their children.
            $sub_limit = TRUE;
          }
          foreach ($file_paths as $file_path) {
            $return_paths[] = $file_path;
          }
        }
      }
    }
  }

  return $return_paths;
}

/**
 * Include files.
 * 
 * Parameters are shared with the following functions:
 * @see hex_path_to_file()
 * @see hex_path_to_files()
 */
function hex_include_files($name, $theme = NULL, $search_theme_parents = FALSE, $regex = FALSE) {
  if ($includes = hex_path_to_files($name, $theme, $search_theme_parents, $regex)) {
    foreach ($includes as $include) {
      include_once $include;
    }
  }
}
