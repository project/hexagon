<?php


/**
 * Get info data specific to a given plug-in. The information will be pulled
 * from the plug-in's .pinfo file.
 * 
 * @param $plugin
 *  (required) The plug-in to target.
 * @param $pinfo_key
 *  (required) The .pinfo key in the plug-in.
 * @param $default
 *  (optional) Default value if none is found.
 */
function hex_plugin_info($plugin, $pinfo_key, $default = NULL) {

  $plugins = _hex_get_cache('plugin');
  if (isset($plugins[$plugin]->info[$pinfo_key])) {
    $plugin_info = $plugins[$plugin]->info[$pinfo_key];
  }

  // Pass through defauts.
  if (!isset($plugin_info)) {
    $plugin_info = $default;
  }

  return $plugin_info;
}

/**
 * Return the path to a given plug-in.
 * 
 * If there are multiple copies of a plug-in with the same name that exists
 * across base themes and sub-themes, the returned path will point to the most
 * active, i.e., the active sub-theme or the sub-theme closest to the current
 * theme hosting the plug-in.
 * 
 * @param $plugin
 *  (required) The plug-in name. If there's no match the function will return
 *  an empty string.
 */
function hex_plugin_path($plugin) {
  $plugins = _hex_get_cache('plugin');
  return isset($plugins[$plugin]) ? dirname($plugins[$plugin]->filename) : NULL;
}

/**
 * Returns a list of all enabled plug-in's.
 */
function hex_plugin_list() {
  $list = array();
  foreach (array_keys(_hex_get_cache('plugin')) as $plugin) {
    $list[$plugin] = $plugin;
  }
  return $list;
}

