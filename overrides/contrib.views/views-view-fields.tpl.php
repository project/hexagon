<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * Inspect $html_elements to see html element data.
 * 
 * @ingroup views_templates
 */
?>
<?php foreach ($fields as $fid => $field): ?>
  <?php if (!empty($field->separator)): ?><?php print $field->separator; ?><?php endif ?>

  <?php print_select($html_elements, "$fid > inline_html#element_open") ?>

    <?php if ($field->label): ?><label class="views-label-<?php print $field->class; ?>"><?php print $field->label; ?>:</label><?php endif; ?>

    <?php print_select($html_elements, "$fid > element_type#element_open") ?>
      <?php print $field->content; ?>
    <?php print_select($html_elements, "$fid > element_type#element_close") ?>

  <?php print_select($html_elements, "$fid > inline_html#element_close") ?>
<?php endforeach; ?>
