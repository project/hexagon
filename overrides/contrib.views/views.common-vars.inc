<?php

/**
 * Phase 2 views fields re-processor.
 */
function hex_preprocess_views_view_fields(&$vars) {

  foreach ($vars['fields'] as $fid => $field) {
    // Default attributes.
    $vars['html_elements'][$fid]['inline_html']['#element_tag'] = $field->inline_html;
    $vars['html_elements'][$fid]['element_type']['#element_tag'] = $field->element_type;
    $vars['html_elements'][$fid]['inline_html']['#attributes']['class'][] = 'views-field-' . $field->class;
    $vars['html_elements'][$fid]['element_type']['#attributes']['class'][] = 'field-content';
  }

}


/**
 * Display the simple view of rows one after another
 */
function hex_preprocess_views_view_unformatted(&$vars) {
  $view = &$vars['view'];
  $rows = &$vars['rows'];

  $vars['html_elements']['rows_title']['#element_tag'] = 'h3';
  $vars['html_elements']['rows_title']['#attributes']['class'][] = 'title';
  $vars['html_elements']['rows_title']['#attributes']['class'][] = 'view-rows-title';

  // Carry over from views defaults.
  foreach (array_keys($rows) as $id) {
    $vars['html_elements']['row_' . $id]['#element_tag'] = 'div';
    if (!empty($vars['classes'][$id])) {
      $vars['html_elements']['row_' . $id]['#attributes']['class'][] = $vars['classes'][$id];
    }
  }

}
