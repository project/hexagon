<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <?php print_select($html_elements, "rows_title#element_open") ?>
    <?php print $title; ?>
  <?php print_select($html_elements, "rows_title#element_close") ?>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php print_select($html_elements, "row_$id#element_open") ?>
    <?php print $row; ?>
  <?php print_select($html_elements, "row_$id#element_close") ?>
<?php endforeach; ?>
