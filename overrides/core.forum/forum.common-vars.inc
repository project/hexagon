<?php
// $Id

/**
 * Process variables to format a forum listing.
 *
 * $variables contains the following information:
 * - $forums
 * - $parents
 * - $tid
 *
 * @see forum-list.tpl.php
 * @see theme_forum_list()
 */
function hex_preprocess_forum_list(&$vars) {
  $html_attributes = &$vars['html_elements']['#attributes'];
  $html_attributes['id'] = 'forum-' . $vars['forum_id'];

  if (empty($vars['disable_sticky'])) {
    drupal_add_js('misc/tableheader.js');
    $html_attributes['class'][] = 'sticky-enabled';
  }
}

/**
 * Preprocess variables to format the topic listing.
 *
 * $variables contains the following data:
 * - $tid
 * - $topics
 * - $sortby
 * - $forum_per_page
 *
 * @see forum-topic-list.tpl.php
 * @see theme_forum_topic_list()
 */
function hex_preprocess_forum_topic_list(&$vars) {

  $html_attributes = &$vars['html_elements']['#attributes'];
  $html_attributes['id'] = 'forum-topic-' . $vars['topic_id'];

  if (empty($vars['disable_sticky'])) {
    drupal_add_js('misc/tableheader.js');
    $html_attributes['class'][] = 'sticky-enabled';
  }
}
