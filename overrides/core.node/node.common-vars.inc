<?php

/**
 * Use this file for template variable processors and any functions called
 * *only* by them.
 */


/**
 * Node template variables preprocessor.
 */
function hex_preprocess_node(&$vars) {
  // Setup ID
  $vars['id_attribute'] = 'node-'. (isset($vars['node']->nid) ? $vars['node']->nid : 'unsaved');
  // Setup classes
  if ($vars['node']->promote) {
    $vars['classes_array'][] = 'promoted';
  }
  if ($vars['sticky']) {
    $vars['classes_array'][] = 'sticky';
  }
  if (!$vars['node']->status) {
    $vars['classes_array'][] = 'unpublished';
  }
  $vars['classes_array'][] = 'node-'. $vars['node']->type;
  if ($vars['node']->uid == $vars['user']->uid && $vars['user']->uid != 0) {
    $vars['classes_array'][] = 'by-viewer';
  }
  if ($vars['teaser']) {
    $vars['classes_array'][] = 'teaser';
    if ($vars['node']->readmore) {
      $vars['classes_array'][] = 'read-more';
    }
  }
}
