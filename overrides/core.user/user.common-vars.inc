<?php


/**
 * Process variables for user-profile-category.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $element
 *
 * @see user-profile-category.tpl.php
 */
function hex_preprocess_user_profile_category(&$vars) {

  // Better context set from category title. No machine readable string available.
  $vars['classes_array'][] = 'category-' . preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '-', strtolower($vars['element']['#title']));

  // For profile category title.
  $vars['html_elements']['category_title']['#element_tag'] = 'h3';
  $title_attr = &$vars['html_elements']['category_title']['#attributes'];
  $title_attr['class'][] = 'title';
  $title_attr['class'][] = 'category-title';

  // For profile category items. Child tag set from hex_preprocess_user_profile_item().
  $vars['html_elements']['category_items']['#element_tag'] = 'dl';
  if (isset($vars['element']['#attributes'])) {
    $item_attr = &$vars['html_elements']['category_items']['#attributes'];
    foreach ($vars['element']['#attributes'] as $attr => $value) {
      if ($attr == 'class') {
        $item_attr['class'][] = $value;
      }
      else {
        $item_attr[$attr] = $value;
      }
    }
  }

}


/**
 * Process variables for user-profile-item.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $element
 *
 * @see user-profile-item.tpl.php
 */
function hex_preprocess_user_profile_item(&$vars) {

  $vars['html_elements']['title']['#element_tag'] = 'dt';
  $vars['html_elements']['value']['#element_tag'] = 'dd';

  if (!empty($vars['element']['#attributes'])) {
    $title_attr = &$vars['html_elements']['title']['#attributes'];
    $value_attr = &$vars['html_elements']['value']['#attributes'];

    foreach ($vars['element']['#attributes'] as $attr => $value) {
      if ($attr == 'class') {
        $title_attr['class'][] = $value;
        $value_attr['class'][] = $value;
      }
      else {
        $title_attr[$attr] = $value;
        $value_attr[$attr] = $value;
      }
    }
  }

}

