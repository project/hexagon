<?php


/**
 * Phase 2 page template variables preprocessor.
 */
function hex_preprocess_page(&$vars) {
  // Form an image element by passing it through theme('image').
  // This will apply a height and width which will render the page more consistently.
  // Since logo's are usually placed on top of the page, not having dimention can have
  // create a inconsistent state for everything below it on low bandwidth connections.
  $vars['logo_img'] = '';
  if (hex_setting('toggle_logo', 1)) {
    if (!hex_setting('default_logo', 1)) {
      // Chop path so theme_image accepts.
      $logo_path = strpos($vars['logo'], base_path()) === 0 ? substr($vars['logo'], strlen(base_path())) : $vars['logo'];
    }
    // Discover the file based on the base name.
    elseif ($logo_path = hex_path_to_file(basename($vars['logo']), hex_active_theme())) {
      // Reset the default logo var.
      $vars['logo'] = base_path() . $logo_path;
    }
    if (!empty($logo_path)) {
      $vars['logo_img']  = theme('image', $logo_path, t('logo'), $vars['site_name'], array('class' => 'logo'));
    }
  }
}

/**
 * Phase 3 page template variables preprocessor.
 */
function hex_re_process_page(&$vars) {
  // Clean out sidebar classes if the expected regions aren't there.
  if (!isset($vars['left']) && !isset($vars['right'])) {
    $vars['body_classes'] = str_replace(' no-sidebars', '', $vars['body_classes']);
  }
  // Carry over default body classes.
  $vars['classes_array'][] = $vars['body_classes'];
}

/**
 * Return a themed set of status and/or error messages. The messages are grouped
 * by type. Converted into a template.
 *
 * @param $display
 *   (optional) Set to 'status' or 'error' to display only messages of that type.
 */
function hex_preprocess_status_messages(&$vars) {
  $vars['messages'] = drupal_get_messages($vars['display']);
  // Sort with 'status' first then 'error'. It's friendlier that way.
  krsort($vars['messages']);
}

/**
 * Return a themed form element. Converted to a template
 *
 * @param element
 *   An associative array containing the properties of the element.
 *   Properties used: title, description, id, required
 * @param $value
 *   The form element's data.
 */
function hex_preprocess_form_element(&$vars) {
  $element = $vars['element'];
  $vars['title']       = !empty($element['#title']) ? filter_xss_admin($element['#title']) : '';
  $vars['description'] = !empty($element['#description']) ? $element['#description'] : '';
  $vars['required']    = !empty($element['#required']) ? $element['#required'] : FALSE;

  if (!empty($element['#id'])) {
    $vars['html_elements']['label']['#attributes']['for'] = $element['#id'];
  }
  $vars['classes_array'][] = 'form-item';
  if (!empty($element['#id'])) {
    $vars['id_attribute'] = $element['#id'] . '-wrapper';
  }
  elseif (!empty($element['#name'])) {
    $name = $element['#name'];
    // Clean out possible brackets.
    if (strpos($name, '[') !== FALSE) {
      $name = str_replace(array('[', ']') ,'' , substr($name, strpos($name, '[')));
    }
    $vars['id_attribute'] = 'name-' . $name . '-wrapper';
  }
  if (isset($element['#attributes']['class'])) {
    // Add a class for this wrapper since there's no way to select parents.
    foreach (explode(' ', $element['#attributes']['class']) as $element_class) {
      $vars['classes_array'][] = $element_class . '-wrapper';
    }
  }
  if (isset($element['#type'])) {
    $vars['classes_array'][] = 'form-' . $element['#type'] . '-wrapper';
    $vars['template files'][] = 'form-element-' . $element['#type'];
  }

  // There tends to be repeats. Use unique values.
  $vars['classes_array'] = array_unique($vars['classes_array']);

}
