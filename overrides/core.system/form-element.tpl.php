<?php
?>
<div<?php print_select($html_elements, '#attributes') ?>>
  <?php if ($title): ?><label<?php print_select($html_elements, 'label#attributes') ?>><?php print $title ?>:<?php if ($required): ?> <em class="form-required"><strong>*</strong> <?php print t('required') ?></em><?php endif ?></label><?php endif ?>
  <?php print $value ?>
  <?php if ($description): ?><div class="description"><?php print $description ?></div><?php endif ?>  
</div>
