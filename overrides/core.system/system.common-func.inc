<?php

/**
 * Use this file for theme *functions*.
 */


/**
 * Changed "div" tag to "p" and removed brackets. Also uses proper ellipses.
 */
function hex_more_help_link($url) {
  return '<p class="more-help-link">'. t('<a href="@link">more help…</a>', array('@link' => check_url($url))) .'</p>';
}

/**
 * Added differentiating class for active menu trails vs. active menu items.
 */
function hex_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if ($in_active_trail) {
    $class .= ($has_children ? ' active-trail' : ' active-menu');
  }
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  return '<li class="'. $class .'">'. $link . $menu ."</li>\n";
}

/**
 * Format a set of radio buttons. Overridden so it always wraps in
 * theme_form_element. This helps consistency in spacing through css.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 *   Properties used: title, value, options, description, required and attributes.
 * @return
 *   A themed HTML string representing the radio button set.
 *
 * @ingroup themeable
 */
function hex_radios($element) {
  $class = 'form-radios';
  if (isset($element['#attributes']['class'])) {
    $class .= ' '. $element['#attributes']['class'];
  }
  $element['#children'] = '<div class="'. $class .'">'. (!empty($element['#children']) ? $element['#children'] : '') .'</div>';

  unset($element['#id']);
  return theme('form_element', $element, $element['#children']);
}

/**
 * Format a set of checkboxes. Overridden so it always wraps in
 * theme_form_element. This helps consistency in spacing through css.
 *
 * @param $element
 *   An associative array containing the properties of the element.
 * @return
 *   A themed HTML string representing the checkbox set.
 *
 * @ingroup themeable
 */
function hex_checkboxes($element) {
  $class = 'form-checkboxes';
  if (isset($element['#attributes']['class'])) {
    $class .= ' '. $element['#attributes']['class'];
  }
  $element['#children'] = '<div class="'. $class .'">'. (!empty($element['#children']) ? $element['#children'] : '') .'</div>';

  unset($element['#id']);
  return theme('form_element', $element, $element['#children']);
}

