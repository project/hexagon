<?php
?>
<?php if ($messages): ?>
<div<?php print_select($html_elements, '#attributes') ?>>
<?php foreach ($messages as $type => $messages): ?>
  <ul class="messages <?php print $type ?>">
  <?php foreach ($messages as $message): ?>
    <li class="message"><?php print $message ?></li>
  <?php endforeach ?>
  </ul>
<?php endforeach ?>
</div>
<?php endif ?>
