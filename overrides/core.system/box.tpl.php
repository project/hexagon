<?php
?>
<div<?php print_select($html_elements, '#attributes') ?>>
  <?php if ($title): ?><h3 class="box-title title"><?php print $title ?></h3><?php endif ?>
  <div class="content"><?php print $content ?></div>
</div>
