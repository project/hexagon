<?php

/**
 * Use this file for template variable processors.
 */


/**
 * Comment template variables preprocessor.
 */
function hex_preprocess_comment(&$vars) {
  // Setup classes
  if (empty($vars['comment']->cid)) {
    $vars['classes_array'][] = 'unsaved';
  }
  if ($vars['comment']->uid == 0) {
    $vars['classes_array'][] = 'by-anonymous';
  }
  else {
    if ($vars['comment']->uid == $vars['node']->uid) {
      $vars['classes_array'][] = 'by-node-author';
    }
    if ($vars['comment']->uid == $vars['user']->uid) {
      $vars['classes_array'][] = 'by-viewer';
    }
  }
  if ($vars['comment']->status) {
    $vars['classes_array'][] = 'unpublished';
  }
  // For read status class. Mapped: 0 = read, 1 = new, 2 = updated.
  $mark = array('read', 'new', 'updated');
  $vars['classes_array'][] = $mark[$vars['comment']->new];
}

/**
 * Comment wrapper template variables preprocessor.
 */
function hex_preprocess_comment_wrapper(&$vars) {
  $vars['classes_array'][] = 'comment-wrapper-' . $vars['node']->type;
}
