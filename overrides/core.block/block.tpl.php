<?php
?>
<div<?php print_select($html_elements, '#attributes') ?>>
  <?php if ($block->subject): ?><h2 class="block-title title"><?php print $block->subject ?></h2><?php endif?>
  <div class="block-content content"><?php print $block->content ?></div>
</div>
