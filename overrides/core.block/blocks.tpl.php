<?php
?>
<?php if ($content): ?>
<div<?php print_select($html_elements, '#attributes') ?>>
  <?php if (!empty($title)): ?><h2 class="region-title title"><?php print $title ?></h2><?php endif ?>
  <?php print $content ?>
</div>
<?php endif ?>
