<?php

/**
 * Phase 2 block region preprocessor.
 */
function hex_preprocess_blocks(&$vars) {
  $vars['content'] = '';
  // ignore_region can be used to prevent loading of the region.
  if (empty($vars['ignore_region']) && $list = block_list($vars['region'])) {
    foreach ($list as $key => $block) {
      // Group into an array for more flexible theming.
      // $key == <i>module</i>_<i>delta</i>
      $vars['content_array'][$key] = theme('block', $block);
    }
  }

  $vars['id_attribute'] = 'blocks-' . $vars['region'];
  $vars['template_files'][] = 'blocks-' . $vars['region'];
}

/**
 * Phase 3 block region reprocessor.
 */
function hex_re_process_blocks(&$vars) {
  // Done late to give other a chance to add to the content.
  if ($set_content = drupal_get_content($vars['region'])) {
    $vars['content_array']['set_content'] = $set_content;
  }
  if (!empty($vars['content_array'])) {
    $vars['content'] = implode("\n", $vars['content_array']);
  }
}
