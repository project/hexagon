<?php

/**
 * Phase 2 block template variables preprocessor.
 */
function hex_preprocess_block(&$vars) {
  // Setup ID
  $vars['id_attribute'] = 'block-' . $vars['block']->module . '-' . $vars['block']->delta;
  // Setup classes
  $vars['classes_array'][] = 'block-' . $vars['block']->module;
}
