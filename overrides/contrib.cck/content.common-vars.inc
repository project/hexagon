<?php
/**
 * Theme preprocess function for content-field.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $node
 * - $field
 * - $items
 * - $teaser
 * - $page
 *
 * @see field.tpl.php
 */
function hex_preprocess_content_field(&$vars) {
  $elements = &$vars['html_elements'];

  $vars['classes_array'][] = 'field';
  $vars['classes_array'][] = 'field-type-' . $vars['field_type_css'];
  $vars['classes_array'][] = 'field-' . $vars['field_name_css'];

  $elements['field-label-above']['#element_tag'] = 'h3';
  $elements['field-label-above']['#attributes']['class'][] = 'label';
  $elements['field-label-above']['#attributes']['class'][] = 'field-label';

  $elements['field-items']['#element_tag'] = 'div';
  $elements['field-items']['#attributes']['class'][] = 'field-items';

  $count = 1;
  foreach ($vars['items'] as $delta => $item) {
    $elements['field-item-' . $delta]['#element_tag'] = 'div';
    $elements['field-item-' . $delta]['#attributes']['class'][] = 'field-item';
    $elements['field-item-' . $delta]['#attributes']['class'][] = $count % 2 ? 'odd' : 'even';
    $elements['field-label-inline-' . $delta]['#attributes']['class'][] = 'label';
    $elements['field-label-inline-' . $delta]['#attributes']['class'][] = 'field-label-inline' . (!$delta ? '-first' : '');
    $count++;
  }

}

/**
 * Process variables for fieldgroup.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $group_name
 * - $group_name_css
 * - $label
 * - $description
 * - $content
 *
 * @see fieldgroup-simple.tpl.php
 */
function hex_preprocess_fieldgroup_simple(&$vars) {
  $elements = &$vars['html_elements'];

  $vars['classes_array'][] = 'fieldgroup';
  $vars['classes_array'][] = 'field' . $vars['group_name_css'];

  $elements['label']['#element_tag'] = 'h2';
  $elements['label']['#attributes']['class'][] = 'label';
  $elements['label']['#attributes']['class'][] = 'fieldgroup-label';

}
