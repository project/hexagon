<?php
/**
 * @file fieldgroup-simple.tpl.php
 * Default theme implementation to display the a 'simple-styled' fieldgroup.
 *
 * Available variables:
 * - $group_name - The group name
 * - $group_name_css - The css-compatible group name.
 * - $label - The group label
 * - $description - The group description
 * - $content - The group content
 *
 * @see template_preprocess_fieldgroup_simple()
 */
?>
<?php if ($content) : ?>
<div<?php print_select($html_elements, '#attributes') ?>>

  <?php if ($label): ?>
    <?php print_select($html_elements, 'label#element_open') ?>
      <?php print $label ?>
    <?php print_select($html_elements, 'label#element_close') ?>
    <?php if ($description): ?>
      <div class="description"><?php print $description; ?></div>
    <?php endif; ?>
  <?php endif; ?>

  <?php print $content; ?>

</div>
<?php endif; ?>
