<?php
/**
 * @file content-field.tpl.php
 * Default theme implementation to display the value of a field.
 *
 * Available variables:
 * - $node: The node object.
 * - $field: The field array.
 * - $items: An array of values for each item in the field array.
 * - $teaser: Whether this is displayed as a teaser.
 * - $page: Whether this is displayed as a page.
 * - $field_name: The field name.
 * - $field_type: The field type.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $label: The item label.
 * - $label_display: Position of label display, inline, above, or hidden.
 * - $field_empty: Whether the field has any valid value.
 *
 * Each $item in $items contains:
 * - 'view' - the themed view for that item
 *
 * @see template_preprocess_field()
 */
?>
<?php if (!$field_empty) : ?>
<div<?php print_select($html_elements, '#attributes') ?>>

  <?php if ($label_display == 'above') : ?>
    <?php print_select($html_elements, 'field-label-above#element_open') ?>
      <?php print t($label) ?>:&nbsp;
    <?php print_select($html_elements, 'field-label-above#element_close') ?>
  <?php endif;?>

  <?php print_select($html_elements, 'field-items#element_open') ?>
    <?php
    foreach ($items as $delta => $item):
      if (!$item['empty']): ?>
        <?php print_select($html_elements, 'field-item-' . $delta . '#element_open') ?>
          <?php if ($label_display == 'inline'): ?>
            <span<?php print_select($html_elements, 'field-label-inline-' . $delta . '#attributes') ?>><?php print t($label) ?>:&nbsp;</span>
          <?php endif; ?>
          <?php print $item['view'] ?>
        <?php print_select($html_elements, 'field-item-' . $delta . '#element_close') ?>
      <?php
      endif;
    endforeach;?>
  <?php print_select($html_elements, 'field-items#element_close') ?>

</div>
<?php endif; ?>
