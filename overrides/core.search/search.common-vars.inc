<?php

/**
 * Process variables for search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $type
 *
 * @see search-result.tpl.php
 */
function hex_preprocess_search_result(&$vars) {

  $vars['html_elements']['title']['#element_tag'] = 'dt';
  $vars['html_elements']['title']['#attributes']['class'][] = 'title';
  $vars['html_elements']['body']['#element_tag'] = 'dd';
  $vars['html_elements']['body']['#attributes']['class'][] = 'result';

}


/**
 * Process variables for search-results.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $results
 * - $type
 *
 * @see search-results.tpl.php
 */
function hex_preprocess_search_results(&$vars) {

  $vars['html_elements']['#element_tag'] = 'dl';
  $vars['html_elements']['#attributes']['class'][] = $vars['type'] . '-results';

}
