<?php


/**
 * Do not modify this file!
 *
 * Variables to avoid since it is in the same scope as phptemplate_init().
 *  - $template
 *  - $file
 */

// Include required files.
$base_includes = array(
  'files.inc',
  'theme.inc',
  'plugin.inc',
  'template.inc',
  'helpers.inc',
);
foreach ($base_includes as $include_file) {
  include_once drupal_get_path('theme', 'hex') . '/includes/' . $include_file;
}
// Invoke initialization function for themes and plug-ins.
foreach (array_merge(hex_active_themes(FALSE), hex_plugin_list()) as $prefix_key) {
  if (function_exists($init_function = $prefix_key . '_init')) {
    $init_function();
  }
}


/**
 * Initialize the theme and rebuild caches when needed.
 */
function hex_init() {
  // See if the theme cache and theme registry needs to be rebuilt.
  if (!hex_cache_is_valid()) {
    // This clears core's theme registry. Force a flush just in case.
    drupal_rebuild_theme_registry();
    // Get the newest data for the resource paths pulled below.
    hex_clear_cache();
    // Search for and include these files:
    //  - template.php
    //  - theme|PLUGIN_NAME.cache.inc
    //  - PLUGIN_NAME.plug.inc
    //  - MODULE_NAME|THEME_NAME|PLUGIN_NAME.common-func.inc
    //  - MODULE_NAME|THEME_NAME|PLUGIN_NAME.common-vars.inc
    //  - HOOK_NAME.func.inc
    //  - HOOK_NAME.vars.inc
    hex_include_files('/^template\.php$|.*\.(cache|plug|(common(-|_))?(func|vars))\.inc$/', NULL, FALSE, TRUE);
    // Rebuild all caches.
    _hex_build_registry();
  }
  else {
    // Search for and include these files:
    //  - PLUGIN_NAME.plug.inc
    //  - MODULE_NAME|THEME_NAME|PLUGIN_NAME.common-func.inc
    //  - HOOK_NAME.func.inc
    hex_include_files('/.*\.(plug|(common(-|_))?func)\.inc$/', NULL, FALSE, TRUE);
  }
}

/**
 * Check to see if the cache is valid.
 * 
 * @param $reset
 *  Get the state again. State is statically cached.
 */
function hex_cache_is_valid($reset = FALSE) {
  static $is_valid;
  if (!isset($is_valid) || $reset) {
    $is_valid =  hex_setting('disable theme caching', 0) == 1 || !cache_get('theme_registry:' . hex_active_theme(), 'cache') ? FALSE : TRUE;
  }
  return $is_valid;
}

/**
 * Load all resource data. Get the data from the other helper functions.
 * 
 * @see hex_path_to_files()
 * @see hex_plugin_info()
 * @see hex_plugin_path()
 * @see hex_plugin_list()
 */
function _hex_get_cache($get, $reset = FALSE) {
  static $resource_data;

  if ($reset && isset($resource_data)) {
    unset($resource_data);
  }
  if (!isset($resource_data)) {
    if (db_is_active() && $resource_cache = cache_get('hex_theme_data:' . hex_active_theme())) {
      $resource_data = $resource_cache->data;
    }
    if (!$resource_data) {
      // Load and save the resource data.
      include_once drupal_get_path('theme', 'hex') . '/includes/theme.cache.inc';
      $resource_data->files = _hex_build_files_data();
      $resource_data->plugin = _hex_get_plugin_data();
      if (db_is_active()) {
        cache_set('hex_theme_data:' . hex_active_theme(), $resource_data);
      }
    }
  }

  return $resource_data->$get;
}

/**
 * Clears the caches designed for hex and its sub-themes.
 */
function hex_clear_cache() {
  if (db_is_active()) {
    cache_clear_all('hex_theme_data', 'cache', TRUE);
  }
}
