
// Taken from Michael Futreal. http://michael.futreal.com/jquery/vjustify
// Modified for min-height. More reliable this way.
jQuery.fn.vjustify=function() {
  // IE6's height property works like min-height in standard browsers.
  var hprop=(jQuery.browser.msie&&jQuery.browser.version<=6)?'height':'min-height';
  var maxHeight=0;
  this.each(function(){
    if ($(this).height()>maxHeight) {
      maxHeight=$(this).height();
    }
  });
  this.each(function(){
    $(this).css(hprop,maxHeight + "px");
    if ($(this).height()>maxHeight) {
      $(this).css(hprop,(maxHeight-($(this).height()-maxHeight))+"px");
    }
  });
};
