
/**
 * jQuery accordion plugin written around Drupal's markup. Specifically block
 * regions but it can be applied elsewhere. Tested with jQuery 1.2.6.
 *
 * This depends on jquery.cookies.js to hold the active state.
 *
 * Written by Joon Park. Licensed under GPL.
 */

jQuery.fn.fxAccordion=function(s) {
  s=jQuery.extend({
    activate: null,                             // Activate on load. Can be a numeric position or a CSS selector recognized by jQuery.
    containerSelect: '.block',                  // The container for each accordion block.
    headerSelect: 'h2.block-title',             // The header for each accordion block.
    contentSelect: '.content',                  // The content for each accordion block.
    highlightTrigger: '.active, .active-trail, .active-menu', // Selectors to search within the content to trigger highlighting.
    vjustify: false                             // Equalize the height for each block.
  },s);

  // Loop through each region.
  $(this).each(function() {
    var allContainers = $(s.containerSelect, this);
    var allContents = $(s.contentSelect, allContainers).wrap('<div class="fx-content"></div>').parent();
    var accordionId = $(this).attr('id');
    var savedId = $.cookie(accordionId);
    var activate = [];

    if (s.vjustify) {
      allContents.vjustify();
    }

    $(this).addClass('fx-accordion');
    allContents.hide();
    if (s.activate !== null) {
      var activate = (typeof(s.activate) == 'number') ? $(s.containerSelect+':eq('+s.activate+')', this) : $(s.activate, this);
      activate.addClass('fx-active').find(s.contentSelect).show();
    }
    if (activate.length == 0 && (savedId !== null || String(savedId).length > 1)) {
      $('#'+savedId).addClass('fx-active').find('.fx-content').show();
    }

    // Loop each block within the block region.
    allContainers.addClass('fx-container').each(function(){
      var container = $(this);
      var blockId = container.attr('id')
      var header = $(s.headerSelect, this);
      if (header.find('a').length === 0) {
        header.wrapInner('<a href="#"></a>');
      }
      if (container.find(s.highlightTrigger).length !== 0) {
        container.addClass('page-active');
      }
      header.addClass('fx-header').find('a').click(function() {
        if ($('#'+blockId+'.fx-active').length ===0) {
          allContainers.removeClass('fx-active').find('.fx-content').filter(":visible").slideToggle('normal');
          $.cookie(accordionId, container.attr('id'), { expires: 30, path: '/'});
        }
        else {
          $.cookie(accordionId, null, { path: '/'});
        }
        container.toggleClass('fx-active').find('.fx-content').slideToggle('normal');
        return false;
      });
    });
  });

  return this;
}
