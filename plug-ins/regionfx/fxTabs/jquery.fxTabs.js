
/**
 * This depends on jquery.accessible_tabs.js.
 *
 * Tested with jQuery 1.2.6
 *
 * Written by Joon Park. Licencesed under GPL.
 */

jQuery.fn.fxTabs=function(s){
  s=jQuery.extend({
    // Most of this is very specific to accessible_tabs. Read the docs there.
    wrapperClass: 'fx-tabs',
    headerSelect: 'h2.block-title',
    headerWrapperClass: 'fx-titles',
    contentSelect: '.block',
    contentClass: 'fx-content',
    currentClass: 'fx-active',
    currentInfoClass: 'fx-current-info',
    currentInfoText: 'current tab: ',
    currentInfoPosition: 'prepend',
    fx:'fadeIn',
    fxspeed: 'normal',
    highlightTrigger: '.active, .active-trail, .active-menu',
    vjustify: true
  },s);

  // Loop through each region.
  $(this).each(function() {
    var container = $(s.contentSelect, this).wrapAll('<div class="'+s.wrapperClass+'"></div>').parent();
    container.accessibleTabs({
      tabhead: s.headerSelect,
      tabheadClass: s.headerWrapperClass,
      tabbody: s.contentSelect,
      wrapperClass: s.contentClass,
      currentClass: s.currentClass,
      currentInfoText: s.currentInfoText,
      currentInfoPosition: s.currentInfoPosition,
      currentInfoClass: s.currentInfoClass,
      fx: s.fx,
      fxspeed: s.fxspeed
    });

    var count = 0;
    var head = $('.'+s.headerWrapperClass, this);
    $(s.contentSelect, container).each(function() {
      if ($(this).find(s.highlightTrigger).length !== 0) {
        $('li', head).filter(':eq('+count+')').addClass('page-active');
        $(this).addClass('page-active');
      }
      count++;
    });

    if (s.vjustify) {
      $('.'+s.contentClass+' '+s.contentSelect, this).vjustify();
    }
  });

  return this;
};
