<?php


/**
 * Block region template variables re-processor.
 */
function regionfx_re_process_blocks(&$vars) {
  // Region fx!
  if (!empty($vars['content']) && $fx_key = regionfx_key($vars['region'])) {
    // Make sure the fx plug-in is enabled.
    if (in_array($fx_key, hex_plugin_info('regionfx', 'active_dependents', array()))) {
      regionfx_attach('#' . hex_filter_attributes('id', $vars['id_attribute']), $fx_key);
    }
  }
}

/**
 * Page template variables re-processor.
 */
function regionfx_preprocess_page(&$vars) {
  // Collect fx scripts. Set from block regions most of the time.
  if (regionfx_get_behaviors()) {
    $vars['scripts'] = drupal_get_js();
  }
}

/**
 * Add settings for behaviors.
 *
 * @param $jq_select
 *  The CSS selector to target the markup. HTML id's are recommended but it
 *  can be anything jQuery understands.
 * @param $fx_key
 *  The plug-in fx to trigger.
 */
function regionfx_attach($jq_select = NULL, $fx_key = NULL) {
  static $settings_added = array();
  if (isset($jq_select) && isset($fx_key)) {
    drupal_add_js(array($fx_key => array($jq_select)), 'setting');
    $settings_added[$fx_key] = $fx_key;
  }
  return $settings_added;
}

/**
 * Collect everything added and include any files that are needed. Do not call
 * directly.
 */
function regionfx_get_behaviors() {
  $fx_added = FALSE;
  if ($fx_attached = regionfx_attach()) {
    foreach ($fx_attached as $fx_key) {
      foreach (hex_plugin_info($fx_key, 'scripts', array()) as $script) {
        if ($script_path = hex_path_to_file($script)) {
          drupal_add_js($script_path, 'theme');
        }
      }
    }
    $script = 'if(Drupal.jsEnabled){$(function(){';
    foreach ($fx_attached as $fx_key) {
      $script .= 'if(typeof(Drupal.settings.' . $fx_key . ')!== \'undefined\'){for(var id in Drupal.settings.' . $fx_key . '){$(Drupal.settings.' . $fx_key . '[id]).' . $fx_key .'();}}';
    }
    $script .= '});}';
    drupal_add_js($script, 'inline');
    $fx_added = TRUE;
  }
  return $fx_added;
}

/**
 * Pass in a region key and it will return the region fx plug-in if it matches.
 */
function regionfx_key($region_key) {
  $fx_key = '';
  foreach (hex_theme_info('regionfx', array()) as $fx_plugin => $regions) {
    if (in_array($region_key, $regions)) {
      $fx_key = $fx_plugin;
      break;
    }
  }
  return $fx_key;
}
