<?php

/**
 * Dynamic style sheet overrides and inclusion.
 */
function smarter_styles_re_process_page(&$vars) {

  /**
   * First pull .info style sheet data and prepare for further processing.
   */
  $enabled_theme_keys = hex_theme_inheritance_for_infokey('inherit stylesheets from');
  $info_css = array();
  // Gather base styles, styles and disabled styles.
  foreach (hex_active_themes(FALSE) as $theme_key) {
    // These info keys used to pull .info.
    foreach (array('stylesheets base' => 'base', 'stylesheets' => 'theme', 'stylesheets disabled' => 'disabled') as $info_key => $type) {
      // Parent theme styles that aren't explicitly set to be inherited will be disabled.
      // This is needed due to Drupal core automatically inheriting parent styles.
      if (!isset($enabled_theme_keys[$theme_key])) {
        if ($type == 'theme') {
          $type = 'disabled';
        }
        else {
          continue;
        }
      }
      foreach (hex_theme_info($info_key, array(), $theme_key) as $media => $styles) {
        foreach ($styles as $i => $style) {
          // Normalize $basename and $style_path. Array data differs.
          if ($info_key == 'stylesheets') {
            $basename = $i;
            $style_path = $style;
          }
          else {
            $basename = basename($style);
            $style_path = hex_theme_path($theme_key) . '/' . $style;
          }
          // If the file exists with the default path assume the themer intended to
          // use what was set. Otherwise, find a matching file on some other path
          // within the theme. Base themes cannot look ahead into a sub-theme.
          if (!file_exists($style_path)) {
            // This can return FALSE so check when processing later.
            $style_path = hex_path_to_file($basename, $theme_key, TRUE);
          }
          $info_css[$media][$basename] = array(
            'basename'   => $basename,
            'group'      => 'main',
            'type'       => $type,
            'path'       => $style_path,
            'preprocess' => TRUE,
          );
        }
      }
    }
  }
  // Restructure API added styles so it's easier to manipulate.
  $added_css = array();
  foreach (drupal_add_css() as $media => $styles) {
    foreach ($styles as $type => $style_paths) {
      foreach ($style_paths as $style_path => $preprocess) {
        $basename = basename($style_path);
        // Filter out RTL styles and .info styles that point to an existing file.
        if (strpos($style_path, '-rtl.css') || !empty($info_css[$media][$basename]['path'])) {
          continue;
        }
        if (!isset($added_css[$media][$basename])) {
          $override_style_path = hex_path_to_file($basename);
          $added_css[$media][$basename] = array(
            'basename'   => $basename,
            'group'      => $preprocess ? 'main' : 'sub',
            'type'       => $type,
            'path'       => $override_style_path ? $override_style_path : $style_path,
            'preprocess' => TRUE,
          );
        }
      }
    }
  }
  // Check for stylesheets associated directly with script files.
  $script_css = array();
  if (hex_setting('attach script styles', 1) == 1 && $scripts = drupal_add_js()) {
    // Media type of 'all, screen' used so this will not be part of the main aggregate.
    // This will be constantly changing and would diminish the benefits of caching if
    // part of the main aggregated css file. Can be changed through .info.
    $media = hex_theme_info('script stylesheet media', 'all');
    foreach (array('core', 'module', 'theme') as $script_type) {
      if (isset($scripts[$script_type])) {
        foreach ($scripts[$script_type] as $script_path => $script_data) {
          $basename = basename($script_path, '.js') . '-js.css';
          // Path to style must exist.
          if ($style_path = hex_path_to_file($basename)) {
            $script_css[$media][$basename] = array(
              'basename'   => $basename,
              'group'      => 'sub',
              // Use the type set from .info if it's set, otherwise default to 'theme'.
              'type'       => isset($info_css[$media][$basename]) ? $info_css[$media][$basename]['type'] : 'theme',
              'path'       => $style_path,
              // Preprocessing styles mirrors the script.
              'preprocess' => $script_data['preprocess'],
            );
          }
        }
      }
    }
  }
  // Flag to work around the 30 linked style sheet limit in IE.
  $ie_debug = FALSE;
  if (hex_setting('ie style link limiter', 0) == 1 && !defined('MAINTENANCE_MODE')) {
    // Make sure aggregation can be enabled.
    $directory = file_directory_path();
    if (is_dir($directory) && is_writable($directory) && (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PUBLIC)) {
      $ie_debug = TRUE;
      // The will be reset back to the site's defaults.
      if (variable_get('preprocess_css', FALSE) !== TRUE) {
        $preprocess_state = FALSE;
        variable_set('preprocess_css', TRUE);
      }
    }
  }

  /**
   * Merge .info styles, API added styles and script bound styles.
   */
  // Start building styles array.
  // The following variable will be pushed through drupal_get_css() to render out the styles.
  $css_reprocess = $css_reprocess_sub = $css_reprocess_ie = array();
  foreach (array('info'           => $info_css,
                 'drupal_add_css' => $added_css,
                 'drupal_add_js'  => $script_css) as $entry_type => $css_group) {
    foreach ($css_group as $media => $styles) {
      foreach ($styles as $basename => $style) {
        if ($style['type'] == 'disabled') {
          continue;
        }
        if ($entry_type == 'info' && !$style['path'] && !empty($added_css[$media][$basename]['path'])) {
          // Swap style path if .info defined style doesn't exist within the theme.
          $style['path'] = $added_css[$media][$basename]['path'];
        }
        elseif ($entry_type == 'drupal_add_js' && isset($info_css[$media][$basename]) && $info_css[$media][$basename]['type']) {
          // Set info defined type if set.
          $style['type'] = $info_css[$media][$basename]['type'];
        }
        if ($ie_debug && ($style['type'] == 'theme' || $style['type'] == 'base')) {
          $style['preprocess'] = FALSE;
        }
        if ($style['group'] == 'main') {
          _smarter_styles_group_add($css_reprocess, $css_reprocess_ie, $media, $style);
        }
        elseif ($style['group'] == 'sub') {
          _smarter_styles_group_add($css_reprocess_sub, $css_reprocess_ie, $media, $style);
        }
      }
    }
  }

  /**
   * Clean and render out back into the $styles variable.
   */
  // Clean up to prevent linking to empty css files processing through drupal_get_css().
  // This can happen aggregation is turned on.
  _smarter_styles_clean_css($css_reprocess);
  _smarter_styles_clean_css($css_reprocess_sub);
  _smarter_styles_clean_css($css_reprocess_ie);
  // Rebuild our final styles.
  $rendered_styles = drupal_get_css($css_reprocess);
  if (!empty($css_reprocess_sub)) {
    $rendered_styles .= drupal_get_css($css_reprocess_sub);
  }
  if (!empty($css_reprocess_ie)) {
    // IE 7 is the cut-off point. Anything below ie 7 should use ie selector hacks for targeting.
    // Can be changed through .info but it will affect all styles which isn't recommended.
    $condition = hex_theme_info('ie conditional expression', 'if lte IE 7');
    $rendered_styles .= "<!--[$condition]>\n" . drupal_get_css($css_reprocess_ie) . "<![endif]-->\n";
  }
  $vars['styles'] = $rendered_styles;

  // Reset back to the default site settings for css aggregation.
  if (isset($preprocess_state)) {
    variable_set('preprocess_css', $preprocess_state);
  }
}

/**
 * Add related styles in one sweep.
 */
function _smarter_styles_group_add(&$css, &$css_ie, $media, $style) {
  // Set proper order by type.
  if (!isset($css[$media])) {
    $css[$media] = array('base' => array(), 'module' => array(), 'theme' => array());
  }
  if (!isset($css_ie[$media])) {
    $css_ie[$media] = array('base' => array(), 'module' => array(), 'theme' => array());
  }
  // Add base style.
  $css[$media][$style['type']][$style['path']] = $style['preprocess'];
  // Add IE style.
  if ($ie_path = _smarter_styles_get_alt($style, '-ie.css')) {
    $css_ie[$media][$style['type']][$ie_path] = $style['preprocess'];
  }
  // Add RTL style and IE RTL style.
  global $language;
  if ($language->direction == LANGUAGE_RTL) {
    if ($rtl_path = _smarter_styles_get_alt($style, '-rtl.css')) {
      $css[$media][$style['type']][$rtl_path] = $style['preprocess'];
    }
    if ($ie_rtl_path = _smarter_styles_get_alt($style, '-ie-rtl.css')) {
      $css_ie[$media][$style['type']][$ie_rtl_path] = $style['preprocess'];
    }
  }
}

/**
 * Helper function to discover alternate styles based on existing ones.
 */
function _smarter_styles_get_alt($style, $suffix) {
  $return_path = '';
  if (file_exists($alt_path = dirname($style['path']) . '/' .  basename($style['basename'], '.css') . $suffix)) {
    $return_path = $alt_path;
  }
  elseif ($alt_path = hex_path_to_file(str_replace('.css', $suffix, $style['basename']))) {
    $return_path = $alt_path;
  }
  return $return_path;
}

/**
 * Clean out empty arrays. This prevents linking to empty CSS files when
 * aggregation is enabled.
 */
function _smarter_styles_clean_css(&$css) {
  foreach ($css as $media => $styles) {
    // Unfortunately, core expects theme styles if module styles are present.
    if (!empty($styles['module']) && empty($styles['theme'])) {
      // This will prevent unsetting of empty theme groups when only the module group exist.
      unset($styles['theme']);
    }
    foreach ($styles as $type => $stylesheets) {
      if (empty($stylesheets)) {
        unset($css[$media][$type]);
      }
    }
    if (empty($css[$media])) {
      unset($css[$media]);
    }
  }
}
