<?php

/**
 * Group multiple block regions into one.
 */
function region_groups_preprocess_page(&$vars) {
  if ($region_groups = hex_theme_info('region groups', array())) {
    foreach ($region_groups as $group_key => $regions) {
      $group_output[$group_key] = '';
      foreach ($regions as $region) {
        if (isset($vars[$region])) {
          $group_output[$group_key] .= $vars[$region];
        }
      }
    }
    foreach ($group_output as $group_key => $group_markup) {
      $vars[$group_key] = $group_markup;
    }
  }
}

